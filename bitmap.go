package main

// BitMap
type BitMap struct {
    bits []byte
    max  int
}

func InitBitMap(max int) *BitMap {
    bits := make([]byte, (max>>3)+1)
    return &BitMap{bits: bits, max: max}
}

func (b *BitMap) Add(num int) {
    index := num >> 3
    pos := num & 0x07
    b.bits[index] |= 1 << pos
}

func (b *BitMap) Remove(num int) {
    index := num >> 3
    pos := num & 0x07
    b.bits[index] = b.bits[index] & ^(1 << pos)
}

func (b *BitMap) IsExist(num int) bool {
    index := num >> 3
    pos := num & 0x07
    return b.bits[index]&(1<<pos) != 0
}