# RangeList

## quick start
```go
go run .
```
you can define your test cases in main.go

## Directory
- bitmap : store bitmap data structures
- utils : store some utils such as Max/Min
- main.go : main function entry
- go.mod : go mod

## what is a bitmap
A structure that describes the state of a piece of data in bits is called a bitmap. bitmap is actually a variation of a hash table. Bitmap can save memory and Bit operations on a bitmap are more efficient.
![bitmap](https://cdn.jsdelivr.net/gh/mouweng/FigureBed/img/202205010040915.png)
## Reference
- [bitmap](https://studygolang.com/articles/18575)